#include <iostream>
#include <string>

using namespace std;

short int count;

template<typename T>
T bwz(T a) {
    return a < 0 ? -a : a;
}

struct Panel {
    short id;
    int x1, y1, x2, y2;
    string data;

    void displayPanel() {
        for (int i = 0; i < data.length(); i++) {
            if (i % bwz(max(x1, x2) - min(x1, x2) + 1) == 0) {
                cout << "\n";
            }
            cout << this->data[i];
        }
        cout << "\n";
    }

    void clearRegion(int x1, int y1, int x2, int y2) {
        for (int i = min(x1, x2); i <= max(x1, x2); i++) {
            for (int j = min(y1, y2); j <= max(y1, y2); j++) {
                data[charIndexFromCoord(i, j)] = '.';
            }
        }
    }

    void drawLine(int x1, int y1, int x2, int y2) {
        if (x1 == x2 && y1 != y2) {
            for (int i = min(y1, y2); i <= max(y1, y2); i++) {
                data[charIndexFromCoord(x1, i)] = 'x';
            }
        } else if (x1 != x2 && y1 == y2) {
            for (int i = min(x1, x2); i <= max(x1, x2); i++) {
                data[charIndexFromCoord(i, y1)] = 'x';
            }
        } else if (x1 != x2 && y1 != y2) {
            for (int i = min(min(y1, y2), min(x1, x2)); i <= max(max(y1, y2), max(x1, x2)); i++) {
                data[charIndexFromCoord(i, i)] = 'x';
            }
        } else {
            data[charIndexFromCoord(y1, y1)] = 'x';
        }
    }

    void drawRectangle(int x1, int y1, int x2, int y2) {
        drawLine(min(x1, x2), min(y1, y2), min(x1, x2), max(y1, y2));
        drawLine(min(x1, x2), min(y1, y2), max(x1, x2), min(y1, y2));
        drawLine(min(x1, x2), max(y1, y2), max(x1, x2), max(y1, y2));
        drawLine(max(x1, x2), min(y1, y2), max(x1, x2), max(y1, y2));
    }

    void copyRegion(int x1, int y1, int x2, int y2, Panel target, int x3, int y3) {
        //TODO
    }

    void joinRegion(int x1, int y1, int x2, int y2, Panel target, int x3, int y3) {
        //TODO
    }

    void moveRegion(int x1, int y1, int x2, int y2, int x3, int y3) {
        //TODO
    }

    void rotateRegion(int x1, int y1, int x2, int y2, int a) {
        switch (a) {
            case 0:
            case 360:
                return;
            case 180:
                int begin = 0, end = data.length() - 1;

                while (begin < end) {
                    char tmp = data[begin];
                    data[begin] = data[end];
                    data[end] = tmp;

                    begin++;
                    end--;
                }
        }
    }

    int charIndexFromCoord(int x, int y) {
        if (x < 0 || y < 0)
            return -1;
        return bwz(max(x1, x2) - min(x1, x2) + 1) * y + x;
    }
};


int findPanel(Panel arr[], int id) {
    for (int i = 0; i < count; i++) {
        if (arr[i].id == id) {
            return i;
        }
    }
    return -1;
}

int main() {
    short id;
    int x1, y1, x2, y2;
    cin >> count;
    Panel panels[count];
    for (int i = 0; i < count; i++) {
        cin >> id >> x1 >> y1 >> x2 >> y2;
        Panel p = {id, x1, y1, x2, y2, ""};
        panels[i] = p;
        for (int j = 0; j <= (max(y1, y2) - min(y1, y2)); j++) {
            for (int k = 0; k <= (max(x1, x2) - min(x1, x2)); k++) {
                panels[i].data += '.';
            }
        }
    }
    string cmd;
    do {
        cin >> cmd;
        int which;
        cin >> which;
        Panel *current = &panels[findPanel(panels, which)];
        if (cmd == "DisplayPanel") {
            current->displayPanel();
        } else if (cmd == "ClearRegion") {
            int x1, x2, y1, y2;
            cin >> x1 >> y1 >> x2 >> y2;
            current->clearRegion(x1, y1, x2, y2);
        } else if (cmd == "DrawLine") {
            int x1, x2, y1, y2;
            cin >> x1 >> y1 >> x2 >> y2;
            current->drawLine(x1, y1, x2, y2);
        } else if (cmd == "DrawRectangle") {
            int x1, x2, y1, y2;
            cin >> x1 >> y1 >> x2 >> y2;
            current->drawRectangle(x1, y1, x2, y2);
//		} else if (cmd=="DrawPoly") {
//			int x1, x2, y1, y2;
//			cin>>x1>>y1>>x2>>y2;
//			current.drawPoly(x1, y1, x2, y2);
//		} else if (cmd=="CopyRegion") {
//			int x1, x2, y1, y2;
//			cin>>x1>>y1>>x2>>y2;
//			current.clearRegion(x1, y1, x2, y2);
//		} else if (cmd=="JoinRegion") {
//			int x1, x2, y1, y2;
//			cin>>x1>>y1>>x2>>y2;
//			current.clearRegion(x1, y1, x2, y2);
//		} else if (cmd=="MoveRegion") {
//			int x1, x2, y1, y2;
//			cin>>x1>>y1>>x2>>y2;
//			current.clearRegion(x1, y1, x2, y2);
//		} else if (cmd=="RotateRegion") {
//			int x1, x2, y1, y2;
//			cin>>x1>>y1>>x2>>y2;
//			current.clearRegion(x1, y1, x2, y2);
        } else if (cmd != "Exit") {
            cout << "Error!" << endl;
        }
//		panels[findPanel(panels, which)] = current;
    } while (cmd != "Exit");
}
